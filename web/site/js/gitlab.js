/*
  MIT License

  Copyright (c) 2022 modweb-js Authors

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

 */
const modOrg = document.getElementById("modOrg").getAttribute("data-mod-org");
const modProject = document.getElementById("modProject").getAttribute("data-mod-project");

const gitlabBaseURL = "https://gitlab.com";
const gitlabAPIURL = `${gitlabBaseURL}/api/v4`;
const filesURL = `${gitlabAPIURL}/projects/${modOrg}%2F${modProject}/packages`;
const projectPath = `${gitlabBaseURL}/${modOrg}/${modProject}`;

const latestVersion = document.getElementById("latestVersion");
const dlLink = document.getElementById("download");
const errors = document.getElementById("errors");
const sha256Link = document.getElementById("sha256");
const sha512Link = document.getElementById("sha512");

function loadURL() {
    const req = new Request(filesURL);
    fetch(req)
        .then(function(response) {
            if (!response.ok) {
                errors.textContent = "There was an error fetching the download links!";
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })

        .then(function(response) {
            const latestPkg = response.pop();
            if (latestVersion)
                latestVersion.textContent = latestPkg.version;
            const latestPkgID = `${latestPkg.id}`;
            const pkgURL = `${gitlabAPIURL}/projects/${modOrg}%2F${modProject}/packages/${latestPkgID}/package_files`;
            const req2 = new Request(pkgURL);

            fetch(req2)
                .then(function(response) {
                    if (!response.ok) {
                        errors.textContent = "There was an error fetching the download links!";
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }
                    return response.json();
                })

                .then(function(response) {
                    dlLink.href = `${projectPath}/-/package_files/${response[0].id}/download`;
                    if (sha256Link)
                        sha256Link.href = `${projectPath}/-/package_files/${response[1].id}/download`;
                    if (sha512Link)
                        sha512Link.href = `${projectPath}/-/package_files/${response[2].id}/download`;
                })
        });
}

window.onload = loadURL();
