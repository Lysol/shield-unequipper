## Shield Unequipper Changelog

#### 2.6

* Added DE localization
* Updated EN localization

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/25127970)

#### 2.5

* Added an interface function so that other mods can temporarily disable this one
* Prevent a script error when a remembered shield is no longer available
* Some small optimizations around accessing settings values

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/24977832)

#### 2.4

* Fix a mod-breaking bug introduced by the last release (properly check for non-weapons)

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/23054888)

#### 2.3

* Fix a bug with wielding non-weapons (e.g., lockpicks/probes)

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/21464245)

#### 2.2

* Fix a bug with starting a new game

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/20994122)

#### 2.1

* Fixed an incompatibility with OpenMW 0.48

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/20778331)

#### 2.0

* Major version bump brings new unequip logic that doesn't favor keeping a shield at all times:
  * If the player had a shield equipped and then equips a relevant weapon, the shield is removed
  * If the player had a relevant weapon equipped and then equips a shield, the weapon is removed
  * This is closer to how the MCP feature behaves
* Fixed a few bugs with remembering shields on save game load
* Added SV localization by Lysol
* Added PT_BR localization by Hurdrax Custos

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/20679914)

#### 1.4

* Packaged the l10n files

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/19722300)

#### 1.3

* Optionally remember your previously-equipped shield and try to re-equip it when appropriate

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/19720171)

#### Version 1.2

* Handled spears
* Trimmed the code down a bit

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/19697602)

#### Version 1.1

* Fixed website typos so that downloads worked (no actual mod changes)

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/19406564)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/shield-unequipper/-/packages/19406518)
